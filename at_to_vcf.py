#!/usr/bin/env python2
#-*-coding:utf-8-*-

f = open('contacts.vcf', 'w')
finput = open('out.txt', 'r')
lines = finput.readlines()
for l in lines:
    if not l:
        continue

    parts = l.split('"')
    phone = parts[1]
    name_16be = parts[3]
    name_str = bytearray.fromhex(name_16be).decode('utf-16-be')
    name_8 = name_str.encode('utf8').encode('hex')
    name_8_required = '=' + ('='.join(a+b for a,b in zip(name_8[::2], name_8[1::2])))

    f.write('BEGIN:VCARD\n')
    f.write('VERSION:2.1\n')
    f.write('N;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:;' + name_8_required + ';;;\n')
    f.write('TEL;CELL:' + phone + '\n')
    f.write('END:VCARD\n')
f.close()
